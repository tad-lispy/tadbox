{
  description = "Tadbox - a silly mailing list service for teaching REST";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-unstable";
  };

  outputs = { self, nixpkgs, ... }@inputs:
    let
      system = "x86_64-linux";
      pkgs = import nixpkgs {
        inherit system;
      };
    in {
      devShells.${system}.default = pkgs.mkShell {
        name = "tadbox-development-shell";
        packages = with pkgs; [
          httpie
          jq
          miniserve
        ];
      };
    };
}
