import { createClient } from 'https://esm.sh/@supabase/supabase-js@2.43.4'

const api_key = `eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6Imp4enRtY3VqZ3NicmdoeGVpZmZyIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MTc3MDg0MDAsImV4cCI6MjAzMzI4NDQwMH0.pIx_Fp0oPoa8ozlKdwr80h3ffWJqUJM3Uv9R9yMU-M4`
const api_root = `https://jxztmcujgsbrghxeiffr.supabase.co`

// Extract information from URL "search" parameters

const url_parameters = new URLSearchParams (location.search)

const inbox = url_parameters.get (`inbox`) || `welcome`
const sender = url_parameters.get (`sender`) || ``

console.debug (`logged in as "${ sender }@${ inbox }"`)


// Handle login dialog and form

const login_dialog = document.querySelector (`#login-dialog`);

window.handle_login_link_click = function (event) {
  event.preventDefault()
  login_dialog.showModal ()
}

window.handle_login_modal_dismissal = function (event) {
  event.preventDefault()
  login_dialog.close ()
}

login_dialog.querySelector (`input[name = "inbox"]`).value = inbox
login_dialog.querySelector (`input[name = "sender"]`).value = sender


// Display login information in the main header

document
  .querySelector (`#main-header slot[name = "sender"]`)
  .replaceWith (sender)

document
  .querySelector (`#main-header slot[name = "inbox"]`)
  .replaceWith (inbox)


// Display messages

const messages = await fetch (`${ api_root }/rest/v1/messages?inbox=eq.${ inbox }`, {
  headers: {
    apikey: api_key,
    Authorization: `Bearer ${ api_key }`
  }
}).then ((response) => response.json ())

const template = document.querySelector (`#message-template`)
const messages_container = document.querySelector (`#messages`)

for (const message of messages) {
  render_message (message)
}


// Subscribe to new messages

const supabase = createClient (api_root, api_key)
supabase
  .channel (`messages`)
  .on (`postgres_changes`, {
    event: `insert`,
    schema: `public`,
    table: `messages`,
    filter: `inbox=eq.${ inbox }`,
  }, handle_message_insertion_event)
  .subscribe ()

function handle_message_insertion_event (payload) {
  render_message (payload.new)
}

// Setup the message form

const message_form = document.querySelector (`#message-form`)

if (sender.trim() === ``) {
  const message = document
        .querySelector (`#not-logged-in-message`)
        .content
        .cloneNode (true)
  message_form.replaceWith (message)
} else {
  message_form.querySelector(`input[type = "submit"]`).disabled = false
}

window.handle_message_form_submission = async function (event) {
  event.preventDefault ()
  const data = new FormData (message_form)
  const body = Object.fromEntries (data.entries ())
  Object.assign (body, {
    sender,
    inbox,
  })

  await fetch (`${ api_root }/rest/v1/messages`, {
    method: `POST`,
    body: JSON.stringify (body),
    headers: {
      "apikey": api_key,
      "Authorization": `Bearer ${ api_key }`,
      "Content-Type": `application/json`,
      "Prefer": `return=representation`,
    }
  }).then ((response) => response.json ())
    // response is an array to allow bulk insert, but we know there is only one message there
    .then ((messages) => messages[0])
    .then (render_message)

  message_form.reset ()
}

function render_message (message) {
  const message_elements = template.content.cloneNode (true)

  for (const name in message) {
    console.debug (`${ name }: ${ message[name] }`)
    const slot = message_elements.querySelector (`slot[name = "${ name }"]`)
    if (slot) {
      slot.replaceWith (message[name])
    }
  }

  messages_container.prepend (message_elements)
}
